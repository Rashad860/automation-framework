/*
 * Copyright (c) 2019 Thermo Fisher Scientific
 * All rights reserved.
 */


package stepdefs;

import java.util.concurrent.TimeUnit;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.PendingException;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import thermo.fisher.testing.Config;


/**
 * TODO: Class description
 */
public class StepDefinitions
{
    /** TODO: Field description */
    WebDriver driver;

    /**
     * TODO: Method description
     *
     * @throws Throwable
     */
    @Given("^i am on pfs login page$")
    public void i_am_on_the_pfs() throws Throwable
    {
        System.setProperty("webdriver.chrome.driver", Config.CHROMEDRIVER);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(Config.HOSTURL);
        driver.manage().window().maximize();
    }

    /**
     * TODO: Method description
     *
     * @throws Throwable
     */
    @When("^i enter username and password$")
    public void i_enter_username_password() throws Throwable
    {
        driver.findElement(By.id("lims_userNameID")).sendKeys("testguy4");
        driver.findElement(By.id("lims_passwordID")).sendKeys("password");

    }

    /**
     * TODO: Method description
     *
     * @throws Throwable
     */
    @And("^i press login button$")
    public void i_press_login() throws Throwable
    {
        driver.findElement(By.id("lims_buttonID")).click();

        synchronized (driver)
        {
            driver.wait(5000);
        }
    }

    /**
     * TODO: Method description
     *
     * @throws Throwable
     */
    @Then("^i should able to login successfully$")
    public void successful_login() throws Throwable
    {
        driver.quit();
    }
}

