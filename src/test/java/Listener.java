import org.testng.*;
import thermo.fisher.testing.slack.WebHook;

import java.util.Map;

public class Listener implements ITestListener, ISuiteListener, IInvokedMethodListener {
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {

    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {

    }

    public void onStart(ISuite iSuite) {
        Reporter.log("About to begin executing Suite " + iSuite.getName(), true);
    }

    public void onFinish(ISuite iSuite) {
        Map<String, ISuiteResult> tests = iSuite.getResults();
        int totalNumberOfFailedMethodsForSuite = 0;
        int totalNumberOfSkippedMethodsForSuite = 0;
        int totalNumberOfPassedMethodsForTest = 0;
        ITestContext overview = null;

        for (ISuiteResult r : tests.values())
        {
            overview = r.getTestContext();
            totalNumberOfFailedMethodsForSuite += overview.getFailedTests().getAllMethods().size();
            totalNumberOfSkippedMethodsForSuite += overview.getSkippedTests().getAllMethods().size();
            totalNumberOfPassedMethodsForTest += overview.getPassedTests().getAllMethods().size();
        }

        String test_summary = "Failed: " + totalNumberOfFailedMethodsForSuite + "\n" +
                              "Skipped: " + totalNumberOfSkippedMethodsForSuite + "\n" +
                              "Passed: " + totalNumberOfPassedMethodsForTest;

        WebHook.sendPostRequest("{\"text\":\"" + test_summary + "\"}");
        Reporter.log("About to end executing Suite " + iSuite.getName(), true);
    }

    public void onTestStart(ITestResult iTestResult) {
        System.out.println("The execution of the main test starts now");
    }

    public void onTestSuccess(ITestResult iTestResult) {
        printTestResults(iTestResult);
    }

    public void onTestFailure(ITestResult iTestResult) {
        printTestResults(iTestResult);
    }

    public void onTestSkipped(ITestResult iTestResult) {
        printTestResults(iTestResult);
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    public void onStart(ITestContext iTestContext) {
        Reporter.log("About to begin executing Test " + iTestContext.getName(), true);
    }

    public void onFinish(ITestContext iTestContext) {
        Reporter.log("Completed executing test " + iTestContext.getName(), true);
    }

    private void printTestResults(ITestResult result) {
        Reporter.log("Test Method resides in " + result.getTestClass().getName(), true);
        if (result.getParameters().length != 0) {
            String params = null;
            for (Object parameter : result.getParameters()) {

                params += parameter.toString() + ",";
            }
            Reporter.log("Test Method had the following parameters : " + params, true);
        }
        String status = null;
        switch (result.getStatus()) {
            case ITestResult.SUCCESS:
                status = "Pass";
                break;
            case ITestResult.FAILURE:
                status = "Failed";
                break;
            case ITestResult.SKIP:
                status = "Skipped";

        }
        Reporter.log("Test Status: " + status, true);
    }
}
