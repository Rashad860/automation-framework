@LoginProfile
Feature: Login Profile
  As an employee of the company
  I want to login my PFS profile using my credentials
  In order to collaborate with my colleagues

  Scenario: login to PFS
    Given i am on pfs login page
    When i enter username and password
    And i press login button
    Then i should able to login successfully