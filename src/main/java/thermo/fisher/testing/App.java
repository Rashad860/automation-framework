package thermo.fisher.testing;

public class App
{
    public static void main( String[] args ) {
        System.out.println("ENVIRONMENTAL VARIABLES:");
        System.out.println("SLACKWEBHOOK: " + Config.SLACKWEBHOOK);
        System.out.println("HOSTURL: " + Config.HOSTURL);
        System.out.println("CHROMEDRIVER: " + Config.CHROMEDRIVER);
    }
}
