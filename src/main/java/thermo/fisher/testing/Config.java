package thermo.fisher.testing;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    private static String filePath = "src\\test\\resources\\config.properties";

    public static String SLACKWEBHOOK = config().getProperty("SLACKWEBHOOK");

    public static String HOSTURL = config().getProperty("HOSTURL");

    public static String CHROMEDRIVER = config().getProperty("CHROMEDRIVER");

    private static Properties config(){
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // load a properties file
        try {
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return prop;
    }
}
